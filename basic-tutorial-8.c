
#include "tutorial.h"

#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <string.h>

#define CHUNK_SIZE 1024
#define SAMPLE_RATE 44100

typedef struct _customData8
{
    GstElement *pipeline;
    GstElement *app_source;
    GstElement *tee;

    GstElement *audio_queue;
    GstElement *audio_convert1;
    GstElement *audio_resample;
    GstElement *audio_sink;

    GstElement *video_queue;
    GstElement *audio_convert2;
    GstElement *visual;
    GstElement *video_convert;
    GstElement *video_sink;

    GstElement *app_queue, *app_sink;

    guint64  num_samples;
    gfloat   a, b, c, d;

    guint sourceid;

    GMainLoop *main_loop;
} CustomData8;

static gboolean push_data(CustomData8 *data)
{
    GstBuffer     *buffer;
    GstFlowReturn  ret;

    int            i;
    GstMapInfo     map;

    gint16        *raw;
    gint           num_samples = CHUNK_SIZE / 2;
    gfloat         freq;

    buffer = gst_buffer_new_and_alloc(CHUNK_SIZE);

    GST_BUFFER_TIMESTAMP(buffer) = gst_util_uint64_scale(data->num_samples, GST_SECOND, SAMPLE_RATE);
    GST_BUFFER_DURATION(buffer)  = gst_util_uint64_scale(num_samples,       GST_SECOND, SAMPLE_RATE);

    gst_buffer_map(buffer, &map, GST_MAP_WRITE);
    raw = (gint16 *)map.data;
    data->c += data->d;
    data->d -= data->c / 1000;
    freq     = 1100 + 1000 * data->d;
    for (i = 0; i < num_samples; ++i)
    {
        data->d += data->b;
        data->b -= data->b / freq;
        raw[i]   = (gint16)(500 * data->a);
    }

    gst_buffer_unmap(buffer, &map);
    data->num_samples += num_samples;

    g_signal_emit_by_name(data->app_source, "push-buffer", buffer, &ret);

    gst_buffer_unref(buffer);

    if (GST_FLOW_OK != ret)
    {
        return FALSE;
    }

    return  TRUE;
}

static void start_feed(GstElement *source, guint size, CustomData8 *data)
{
    if (data->sourceid == 0)
    {
        g_print("  start feeding...\n");
        data->sourceid = g_idle_add((GSourceFunc)push_data, data);
    }
}

static void stop_feed(GstElement *source, CustomData8 *data)
{
    if (data->sourceid != 0)
    {
        g_print("  stop feeding ...\n");
        g_source_remove(data->sourceid);
        data->sourceid = 0;
    }
}

static GstFlowReturn new_sample(GstElement *sink, CustomData8 *data)
{
    GstSample *sample;

    g_signal_emit_by_name(sink, "pull-sample", &sample);
    if (sample)
    {
        g_print("*");
        gst_sample_unref(sample);
        return GST_FLOW_OK;
    }

    return GST_FLOW_ERROR;
}

static void error_cb(GstBus *bus, GstMessage *message, CustomData8 *data)
{
    GError *error;
    gchar  *debug_info;

    gst_message_parse_error(message, &error, &debug_info);
    g_printerr("error received from element %s: %s\n", GST_OBJECT_NAME(message->src), error->message);
    g_printerr("debug info: %s\n", debug_info ? debug_info : "none");

    g_clear_error(&error);
    g_free(debug_info);

    g_main_loop_quit(data->main_loop);
}

static gboolean element_create_and_check(CustomData8 *data)
{
    data->app_source = gst_element_factory_make("appsrc", "audio_source");
    if (!data->app_source)
    {
        g_printerr("'appsrc' element can't not create\n");
        return FALSE;
    }

    data->tee = gst_element_factory_make("tee", "tee");
    if (!data->tee)
    {
        g_printerr("'tee' element can't not create\n");
        return FALSE;
    }

    data->audio_queue = gst_element_factory_make("queue", "audio_queue");
    if (!data->audio_queue)
    {
        g_printerr("'audio_queue' element can't not create\n");
        return FALSE;
    }

    data->audio_convert1 = gst_element_factory_make("audioconvert", "audio_convert1");
    if (!data->audio_convert1)
    {
        g_printerr("'audio_convert1' element can't not create\n");
        return FALSE;
    }

    data->audio_resample = gst_element_factory_make("audioresample", "audio_resample");
    if (!data->audio_resample)
    {
        g_printerr("'audio_resample' element can't not create\n");
        return FALSE;
    }

    data->audio_sink = gst_element_factory_make("autoaudiosink", "audio_sink");
    if (!data->audio_sink)
    {
        g_printerr("'audio_sink' element can't not create\n");
        return FALSE;
    }

    data->video_queue = gst_element_factory_make("queue", "video_queue");
    if (!data->video_queue)
    {
        g_printerr("'video_queue' element can't not create\n");
        return FALSE;
    }

    data->audio_convert2 = gst_element_factory_make("audioconvert", "audio_convert2");
    if (!data->audio_convert2)
    {
        g_printerr("'audio_convert2' element can't not create\n");
        return FALSE;
    }

    data->visual = gst_element_factory_make("wavescope", "visual");
    if (!data->audio_convert2)
    {
        g_printerr("'visual' element can't not create\n");
        return FALSE;
    }

    data->video_convert = gst_element_factory_make("videoconvert", "video_convert");
    if (!data->video_convert)
    {
        g_printerr("'video_convert' element can't not create\n");
        return FALSE;
    }

    data->video_sink = gst_element_factory_make("autovideosink", "video_sink");
    if (!data->video_sink)
    {
        g_printerr("'video_sink' element can't not create\n");
        return FALSE;
    }

    data->app_queue = gst_element_factory_make("queue", "app_queue");
    if (!data->app_queue)
    {
        g_printerr("'app_queue' element can't not create\n");
        return FALSE;
    }

    data->app_sink = gst_element_factory_make("appsink", "app_sink");
    if (!data->app_sink)
    {
        g_printerr("'app_sink' element can't not create\n");
        return FALSE;
    }

    data->pipeline = gst_pipeline_new("test-pipeline");
    if (!data->pipeline)
    {
        g_printerr("'pipeline' element can't not create\n");
        return FALSE;
    }

    return TRUE;
}

int base_tutorial8(int argc, char *argv[])
{
    CustomData8 data;

    GstPad *tee_audio_pad,   *tee_video_pad,   *tee_app_pad;
    GstPad *queue_audio_pad, *queue_video_pad, *queue_app_pad;

    GstAudioInfo  info;
    GstCaps      *audio_caps;
    GstBus       *bus;

    memset(&data, 0, sizeof(data));
    data.b = 1;
    data.d = 1;

    gst_init(&argc, &argv);

    if(!element_create_and_check(&data))
    {
        g_printerr("not all element created.\n");
        return -1;
    }

    /* Configure wavescope */
    g_object_set(data.visual, "shader", 0, "style", 0, NULL);

    /* Configure appsrc */
    gst_audio_info_set_format(&info, GST_AUDIO_FORMAT_S16, SAMPLE_RATE, 1, NULL);
    audio_caps = gst_audio_info_to_caps(&info);
    g_object_set(data.app_source, "caps", audio_caps, "format", GST_FORMAT_TIME, NULL);
    g_signal_connect(data.app_source, "need-data", G_CALLBACK(start_feed), &data);
    g_signal_connect(data.app_source, "enough-data", G_CALLBACK(stop_feed), &data);

    /* Configure appsink */
    g_object_set(data.app_sink, "emit-signals", TRUE, "caps", audio_caps, NULL);
    g_signal_connect(data.app_sink, "new-sample", G_CALLBACK (new_sample), &data);

    gst_caps_unref(audio_caps);

    gst_bin_add_many( GST_BIN(data.pipeline),
                      data.app_source,
                      data.tee,
                      data.audio_queue,
                      data.audio_convert1,
                      data.audio_resample,
                      data.audio_sink,
                      data.video_queue,
                      data.audio_convert2,
                      data.visual,
                      data.video_convert,
                      data.video_sink,
                      data.app_queue,
                      data.app_sink,
                      NULL);

    if (gst_element_link_many (data.app_source, data.tee, NULL) != TRUE)
    {
        g_printerr ("'app_source' can not add link 'tee'.\n");
        gst_object_unref (data.pipeline);
        return -1;
    }

    if ( gst_element_link_many( data.audio_queue,
                                data.audio_convert1,
                                data.audio_resample,
                                data.audio_sink,
                                NULL) != TRUE)
    {
        g_printerr ("'audio_queue' can not add link 'tee'.\n");
        gst_object_unref (data.pipeline);
        return -1;
    }

    if( gst_element_link_many( data.video_queue,
                               data.audio_convert2,
                               data.visual,
                               data.video_convert,
                               data.video_sink,
                               NULL) != TRUE)
    {
        g_printerr ("'audio_queue' can not add link '...'.\n");
        gst_object_unref (data.pipeline);
        return -1;
    }

    if(gst_element_link_many (data.app_queue, data.app_sink, NULL) != TRUE)
    {
        g_printerr ("'app_queue' can not add link 'app_sink'.\n");
        gst_object_unref (data.pipeline);
        return -1;
    }

    /* Manually link the Tee, which has "Request" pads */
    tee_audio_pad   = gst_element_get_request_pad (data.tee, "src_%u");
    g_print ("Obtained request pad %s for audio branch.\n", gst_pad_get_name (tee_audio_pad));

    queue_audio_pad = gst_element_get_static_pad (data.audio_queue, "sink");

    tee_video_pad   = gst_element_get_request_pad (data.tee, "src_%u");
    g_print ("Obtained request pad %s for video branch.\n", gst_pad_get_name (tee_video_pad));

    queue_video_pad = gst_element_get_static_pad (data.video_queue, "sink");

    tee_app_pad     = gst_element_get_request_pad (data.tee, "src_%u");
    g_print ("Obtained request pad %s for app branch.\n", gst_pad_get_name (tee_app_pad));

    queue_app_pad = gst_element_get_static_pad (data.app_queue, "sink");
    if (gst_pad_link (tee_audio_pad, queue_audio_pad) != GST_PAD_LINK_OK ||
        gst_pad_link (tee_video_pad, queue_video_pad) != GST_PAD_LINK_OK ||
        gst_pad_link (tee_app_pad, queue_app_pad) != GST_PAD_LINK_OK)
    {
        g_printerr ("Tee could not be linked\n");
        gst_object_unref (data.pipeline);
        return -1;
    }
    gst_object_unref (queue_audio_pad);
    gst_object_unref (queue_video_pad);
    gst_object_unref (queue_app_pad);

    /* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
    bus = gst_element_get_bus (data.pipeline);
    gst_bus_add_signal_watch (bus);
    g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)error_cb, &data);
    gst_object_unref (bus);

    /* Start playing the pipeline */
    gst_element_set_state (data.pipeline, GST_STATE_PLAYING);

    /* Create a GLib Main Loop and set it to run */
    data.main_loop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (data.main_loop);

    /* Release the request pads from the Tee, and unref them */
    gst_element_release_request_pad (data.tee, tee_audio_pad);
    gst_element_release_request_pad (data.tee, tee_video_pad);
    gst_element_release_request_pad (data.tee, tee_app_pad);
    gst_object_unref (tee_audio_pad);
    gst_object_unref (tee_video_pad);
    gst_object_unref (tee_app_pad);

    /* Free resources */
    gst_element_set_state (data.pipeline, GST_STATE_NULL);
    gst_object_unref (data.pipeline);
    return 0;
}

