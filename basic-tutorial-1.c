//
//
//

#include <gst/gst.h>

int base_tutorial1(int argc, char *argv[]) {

    GstElement *pipeline;
    GstBus *bus;
    GstMessage *message;

    gst_init(&argc, &argv);

    pipeline = gst_parse_launch("playbin3 uri=rtmp://live.hkstv.hk.lxdns.com/live/hks", NULL);

    gst_element_set_state(pipeline, GST_STATE_PLAYING);

    bus = gst_element_get_bus(pipeline);
    message = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
    if (NULL != message)
    {
        gst_message_unref(message);
    }

    gst_object_unref(bus);
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);

    return 0;
}
