
#include <stdio.h>
#include <memory.h>

#include "tutorial.h"

void use_help(char *argv[])
{
    printf("%s useage:\n", argv[0]);
    printf("\t arg[1]  1: 'Basic tutorial  1: Hello world!'\n");
    printf("\t arg[1]  2: 'Basic tutorial  2: GStreamer concepts'\n");
    printf("\t arg[1]  3: 'Basic tutorial  3: Dynamic Hello World'\n");
    printf("\t arg[1]  4: 'Basic tutorial  4: Time management'\n");
    printf("\t arg[1]  5: 'Basic tutorial  5: GUI toolkit integration'\n");
    printf("\t arg[1]  6: 'Basic tutorial  6: Media formats and Pad Capabilities'\n");
    printf("\t arg[1]  7: 'Basic tutorial  7: Multithreading and Pad Availability'\n");
    printf("\t arg[1]  8: 'Basic tutorial  8: Short-cutting the pipeline'\n");
    printf("\t arg[1]  9: 'Basic tutorial  9: Media information gathering'\n");
    printf("\t arg[1] 12: 'Basic tutorial 12: Streaming'\n");
    printf("\t arg[1] 13: 'Basic tutorial 13: Playback speed'\n");
}

int main(int argc, char *argv[])
{
    int result = 0;

    if (argc <= 1)
    {
        use_help(argv);
        return 0;
    }

    const char *type = argv[1];

    if (0 == strcmp("1", type))
    {
        result = base_tutorial1(argc, argv);
    }
    else if (0 == strcmp("2", type))
    {
        result = base_tutorial2(argc, argv);
    }
    else if (0 == strcmp("3", type))
    {
        result = base_tutorial3(argc, argv);
    }
    else if (0 == strcmp("4", type))
    {
        result = base_tutorial4(argc, argv);
    }
    else if (0 == strcmp("5", type))
    {
        result = base_tutorial5(argc, argv);
    }
    else if (0 == strcmp("6", type))
    {
        result = base_tutorial6(argc, argv);
    }
    else if (0 == strcmp("7", type))
    {
        result = base_tutorial7(argc, argv);
    }
    else if (0 == strcmp("8", type))
    {
        result = base_tutorial8(argc, argv);
    }
    else if (0 == strcmp("9", type))
    {
        result = base_tutorial9(argc, argv);
    }
    else if (0 == strcmp("12", type))
    {
        result = base_tutorial12(argc, argv);
    }
    else if (0 == strcmp("13", type))
    {
        result = base_tutorial13(argc, argv);
    }

    return result;
}
