
#include "tutorial.h"
#include <gst/gst.h>

typedef struct CustomData12
{
    gboolean    is_live;
    GstElement *pipeline;
    GMainLoop  *loop;
} CustomData12;

static void cb_message(GstBus *bus, GstMessage *message, CustomData12 *data)
{
    switch (GST_MESSAGE_TYPE(message))
    {
        case GST_MESSAGE_ERROR:
            {
                GError  *error;
                gchar   *debug;

                gst_message_parse_error(message, &error, &debug);
                g_print("Error: %s\n", error->message);
                g_error_free(error);
                g_free(debug);

                gst_element_set_state(data->pipeline, GST_STATE_READY);
                g_main_loop_quit(data->loop);

                break;
            }

        case GST_MESSAGE_EOS:
            {
                gst_element_set_state(data->pipeline, GST_STATE_READY);
                g_main_loop_quit(data->loop);
                break;
            }

        case GST_MESSAGE_BUFFERING:
            {
                gint  percent = 0;

                if(data->is_live) break;

                gst_message_parse_buffering(message, &percent);
                g_print("Buffering (%3d%%)\r", percent);

                if (percent < 100)
                {
                    gst_element_set_state(data->pipeline, GST_STATE_PAUSED);
                }
                else
                {
                    gst_element_set_state(data->pipeline, GST_STATE_PLAYING);
                }

                break;
            }

        case GST_MESSAGE_CLOCK_LOST:
            {
                gst_element_set_state (data->pipeline, GST_STATE_PAUSED);
                gst_element_set_state (data->pipeline, GST_STATE_PLAYING);
                break;
            }

        default:
            break;
    }
}

int base_tutorial12(int argc, char *argv[])
{
    GstElement           *pipeline;
    GstBus               *bus;
    GstStateChangeReturn  ret;
    GMainLoop            *main_loop;
    CustomData12          data;
    gchar                *uri;
    gchar                *play_uri;

    gst_init(&argc, &argv);

    memset(&data, 0x00, sizeof(data));

    if (argc > 2)
    {
        uri = argv[2];
    }
    else
    {
        uri = "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm";
    }

    play_uri = g_strdup_printf("playbin uri=%s", uri);
    pipeline = gst_parse_launch(play_uri, NULL);
    bus      = gst_element_get_bus(pipeline);
    ret      = gst_element_set_state(pipeline, GST_STATE_PLAYING);

    if (ret == GST_STATE_CHANGE_FAILURE)
    {
        g_printerr("Unable to set the pipeline to the playing state");
        gst_object_unref(pipeline);
        g_free(play_uri);
        return -1;
    }
    else if (ret == GST_STATE_CHANGE_NO_PREROLL)
    {
        data.is_live = TRUE;
    }

    main_loop = g_main_loop_new(NULL, FALSE);
    data.loop     = main_loop;
    data.pipeline = pipeline;

    gst_bus_add_signal_watch(bus);
    g_signal_connect(bus, "message", G_CALLBACK(cb_message), &data);

    g_main_loop_run(main_loop);

    g_main_loop_unref(main_loop);
    gst_object_unref(bus);
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);

    g_free(play_uri);

    return 0;
}

