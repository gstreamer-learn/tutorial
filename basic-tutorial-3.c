
#include "tutorial.h"

#include <gst/gst.h>


typedef struct _CustomData {
    GstElement *pipeline;
    GstElement *source;
    GstElement *convert;
    GstElement *sink;
} CustomData;

static void pad_added_handler(GstElement *src, GstPad *pad, CustomData *data);

static const char *default_uri = "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm";
//const char *default_uri = "rtmp://live.hkstv.hk.lxdns.com/live/hks";

int base_tutorial3(int argc, char *argv[]) {
    CustomData data;
    GstBus *bus;
    GstMessage *message;
    GstStateChangeReturn ret;
    gboolean terminate = FALSE;
    const char *uri = NULL;

    gst_init(&argc, &argv);

    // call like:
    // ./abc 3 http://xxx.mp3
    if (argc >= 3)
    {
        uri = argv[2];
    }
    else
    {
        uri = default_uri;
    }


    data.source = gst_element_factory_make("uridecodebin", "source");
    data.convert = gst_element_factory_make("audioconvert", "convert");
    data.sink = gst_element_factory_make("autoaudiosink", "sink");

    data.pipeline = gst_pipeline_new("my-test-pipeline");

    if (!data.source || !data.convert || !data.sink || !data.pipeline)
    {
        g_printerr("Not all element could be build\n");
        return -1;
    }


    gst_bin_add_many(GST_BIN(data.pipeline), data.source, data.convert, data.sink, NULL);

    if (!gst_element_link(data.convert, data.sink))
    {
        g_printerr("element could not be linked.\n");
        gst_object_unref(data.pipeline);
        return -1;
    }

    g_object_set(data.source, "uri", uri, NULL);

    g_signal_connect(data.source, "pad-added", G_CALLBACK(pad_added_handler), &data);

    ret = gst_element_set_state(data.pipeline, GST_STATE_PLAYING);
    if (GST_STATE_CHANGE_FAILURE == ret)
    {
        g_printerr("unable to set the pipe to playing state\n");
        gst_object_unref(data.pipeline);
        return -1;
    }

    bus = gst_element_get_bus(data.pipeline);

    do
    {
        message = gst_bus_timed_pop_filtered(
                bus,
                GST_CLOCK_TIME_NONE,
                GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS
        );

        if (NULL != message)
        {
            GError *error;
            gchar *debug_info;

            switch (GST_MESSAGE_TYPE(message))
            {
                case GST_MESSAGE_STATE_CHANGED:
                {
                    if (GST_MESSAGE_SRC(message) == GST_OBJECT (data.pipeline))
                    {
                        GstState old_state, new_state, pending_state;
                        gst_message_parse_state_changed(message, &old_state, &new_state, &pending_state);
                        g_print("Pipeline state changed from %s to %s:\n",
                                gst_element_state_get_name(old_state),
                                gst_element_state_get_name(new_state));
                    }
                    break;
                }

                case GST_MESSAGE_ERROR:
                {
                    gst_message_parse_error(message, &error, &debug_info);
                    g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME (message->src), error->message);
                    g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
                    g_clear_error(&error);
                    g_free(debug_info);
                    terminate = TRUE;
                    break;
                }

                case GST_MESSAGE_EOS:
                {
                    g_print("End-Of-Stream reached.\n");
                    terminate = TRUE;
                    break;
                }

                default:
                {
                    /* We should not reach here */
                    g_printerr("Unexpected message received.\n");
                    break;
                }
            }

            gst_message_unref(message);
        }
    }
    while (!terminate);


    /* Free resources */
    gst_object_unref(bus);
    gst_element_set_state(data.pipeline, GST_STATE_NULL);
    gst_object_unref(data.pipeline);

    return 0;
}


/* This function will be called by the pad-added signal */
static void pad_added_handler(GstElement *src, GstPad *new_pad, CustomData *data) {
    GstPad *sink_pad = gst_element_get_static_pad(data->convert, "sink");
    GstPadLinkReturn ret;
    GstCaps *new_pad_caps = NULL;
    GstStructure *new_pad_struct = NULL;
    const gchar *new_pad_type = NULL;

    g_print("Received new pad '%s' from '%s':\n", GST_PAD_NAME (new_pad), GST_ELEMENT_NAME (src));

    /* If our converter is already linked, we have nothing to do here */
    if (gst_pad_is_linked(sink_pad))
    {
        g_print("We are already linked. Ignoring.\n");
        goto exit;
    }

    /* Check the new pad's type */
    new_pad_caps = gst_pad_get_current_caps(new_pad);
    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0);
    new_pad_type = gst_structure_get_name(new_pad_struct);
    if (!g_str_has_prefix(new_pad_type, "audio/x-raw"))
    {
        g_print("It has type '%s' which is not raw audio. Ignoring.\n", new_pad_type);
        goto exit;
    }

    /* Attempt the link */
    ret = gst_pad_link(new_pad, sink_pad);
    if (GST_PAD_LINK_FAILED (ret))
    {
        g_print("Type is '%s' but link failed.\n", new_pad_type);
    }
    else
    {
        g_print("Link succeeded (type '%s').\n", new_pad_type);
    }

    exit:
    /* Unreference the new pad's caps, if we got them */
    if (new_pad_caps != NULL)
        gst_caps_unref(new_pad_caps);

    /* Unreference the sink pad */
    gst_object_unref(sink_pad);
}
